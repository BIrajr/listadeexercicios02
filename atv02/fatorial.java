package atv02;

import java.math.BigInteger;
import java.util.Scanner;

public class fatorial {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		int aux = entrada();
		BigInteger num = BigInteger.valueOf(aux);
		saida(calculo(num));
	}

	public static int entrada() {
		System.out.println("Digite um numero entre 0 e 100:");
		return sc.nextInt();
	}

	public static BigInteger calculo(BigInteger num) {
		BigInteger aux = num;
		for (int i = num.intValue(); i > 0; i--) {
		    num = aux.multiply(BigInteger.valueOf(i));
		    System.out.println(i + "! = " + num);
		   
		}
		
		return num;
	}

	public static void saida(BigInteger num) {
		System.out.println("Resultado: " + num);
	}
}
